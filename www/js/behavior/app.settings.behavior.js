$(function() {
	window.App.view.settings.behavior = function() {
		var app = window.App;
		
		$.ajaxProxy({
			url : "users/" + window.App.user.id + ".json",
			callback : function(d) {
			window.App.user = d.user.User;
			var user = app.user;
			
			$('#settings [data-role="content"]').html($.template('settings', user));
	
			function buildSlider($el) {
				$el.switchButton({
					width : 100,
					height : 20,
					button_width : 20
				});
			}
	
			function connectionString(connected) {
				return '<input type="checkbox" value="1" ' + ( connected ? 'checked' : '') + ' />';
			}
			
			//LOCATION SERVICES
			$('.status.location-services').html(connectionString(window.App.userApprovedLocationServices));
			buildSlider($('.status.location-services input'));
			
			$('.status.location-services .switch-button-background, .status.location-services .switch-button-label').click(function() {
				if ($(this).parent().find('.switch-button-background').hasClass('checked')) {
					navigator.geolocation.getCurrentPosition(function(currentPosition) {
						if (window.plugins) {
							window.plugins.backgroundGeoLocation.start();	
						}
						window.App.userApprovedLocationServices = true;
					}, function(e) {
						$.notify(window.App.view.global.labels.locationUnableError);
						if (window.plugins) {
							window.plugins.backgroundGeoLocation.stop();	
						}
						window.App.userApprovedLocationServices = false;
						$('.status.location-services .switch-button-background').trigger('click');
					}, {
						timeout : 10000,
						enableHighAccuracy : true,
						maximumAge : 75000
					});
				} else {
					if (window.plugins) {
						window.plugins.backgroundGeoLocation.stop();	
					}
					window.App.userApprovedLocationServices = false;
				}
			});
	
			//FACEBOOK
			$.checkFacebookStatus(function(status) {
				$('.status.facebook').html(connectionString(status));
				buildSlider($('.status.facebook input'));
			});
			$('.status.facebook .switch-button-background, .status.facebook .switch-button-label').click(function() {
				if ($(this).parent().find('.switch-button-background').hasClass('checked')) {
					$.facebookLogin(function(connected) {
						if(!connected){
							$('.status.facebook .switch-button-background').trigger('click');
						}
					});
				} else {
					$.facebookLogoff(function() {
					});
				}
			});
	
			//GOOGLE
			/*$.checkGoogleStatus(function(status) {
				$('.status.google').html(connectionString(status));
				buildSlider($('.status.google input'));
			});
	
			$('.status.google .switch-button-background, .status.google .switch-button-label').click(function() {
				if ($(this).parent().find('.switch-button-background').hasClass('checked')) {
					$.googleLogin(function(connected) {
						if(!connected){
							$('.status.google .switch-button-background').trigger('click');
						}
					});
				} else {
					$.googleLogoff(function() {
					});
				}
			});*/
	
			//TWITTER
			$.checkTwitterStatus(function(status) {
				$('.status.twitter').html(connectionString(status));
				buildSlider($('.status.twitter input'));
			});
	
			$('.status.twitter .switch-button-background, .status.twitter .switch-button-label').click(function() {
				if ($(this).parent().find('.switch-button-background').hasClass('checked')) {
					$.twitterLogin(function(connected) {
						if(!connected){
							$('.status.twitter .switch-button-background').trigger('click');
						}
					});
				} else {
					$.twitterLogoff(function() {
					});
				}
			});
	
			//INSTAGRAM
			/*$.checkInstagramStatus(function(status) {
				$('.status.instagram').html(connectionString(status));
				buildSlider($('.status.instagram input'));
			});
	
			$('.status.instagram .switch-button-background, .status.instagram .switch-button-label').click(function() {
				if ($(this).parent().find('.switch-button-background').hasClass('checked')) {
					$.instagramLogin(function(connected) {
						if(!connected){
							$('.status.instagram .switch-button-background').trigger('click');
						}
					});
				} else {
					$.instagramLogoff(function() {});
				}
			});*/
	
			function applyUser(u) {
				$("#settings .profile-image").css('background-image', 'url(' + u.featured_image + ')');
				$('.username').html($.truncate(u.username.toUpperCase(), 6));
	
				for (var i in u) {
					var $f = $('[name="' + i + '"]');
					if ($f.attr('type') != 'file') {
						$('[name="' + i + '"]').val(u[i]);
					}
				}
			}
	
			$('.btnSubmit').click(function() {
				$('#profile-form').ajaxFormSubmit(function(data) {
					var obj = {};
					$.each(data, function(i, item) {
						obj[item.name] = item.value;
					});
					obj.active = 1;
					$.ajaxProxy({
						url : "/users/" + obj['id'] + ".json",
						type : 'post',
						data : obj,
						callback : function(re) {
							for (var i in obj) {
								user[i] = obj[i];
							}
							$.saveLoginDetails(user);
							applyUser(user);
						}
					});
				});
			});
			
			$("#settings .profile-image").click(function(){
				$.avatarChooser(function(bytes){
					$('#settings .featured_image').val(bytes);
					$("#settings .profile-image").css('background-image', 'url("' + bytes + '")');
				
				});
			});
	
			$('#featured_image_mask').click(function() {
				$('#featured_image').trigger("click");
			});
	
			$('#featured_image').change(function() {
				$('#featured_image_mask').val($(this).val());
			});
	
			$('#settings').trigger('create');
	
			$('#featured_image').parent().hide();
	
			$('#settings').trigger('create');
			
			applyUser(user);
		}
		});
		return {};
	};
});
