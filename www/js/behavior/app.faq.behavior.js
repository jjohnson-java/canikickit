$(function() {
	window.App.view.faq.behavior = function() {
		$.ajaxProxy({
			url : "faqs.json",
			callback : function(d) {
				var html = '';
				$.each(d.rows, function(i, item) {
					var p = item;
					html += '<li>';
					html += '<a href="javascript:void(0);" data-id="' + p.id + '" >' + p.question + '</a>';
					html += '</li>';
				});
				$('#faq [data-role="listview"]').html(html).listview('refresh').attr('data-loaded', true);

				$('#faq [data-role="listview"] a').click(function() {
					var id = $(this).attr('data-id');
					$.each(d.rows, function(i, item) {
						var p = item;
						if (p.id == id) {
							$.overlay('faq-overlay', p, function() {

							});
							return false;
						}
					});
				});
			}
		});
		return {};
	};
});
