$(function() {
	window.App.view.overview.behavior = function() {
		console.log('here');
		$.ajaxProxy({
			url : "contributors/overview",
			type : 'POST',
			data : {
				location : {},
				keyword : ""
			},
			callback : function(d) {
				d.establishments = [];
				d.rankingContibutors = [];
				
				$.each(d.contributors, function(){
					if(this.User.showin_ui == 0){
						d.establishments.push(this);
					}else{
						d.rankingContibutors.push(this);	
					}
				});

				$('#overview [data-role="contributor-browser"]').contributorBrowser(d);
				$('#overview [data-role="establishment-browser"]').establishmentBrowser(d);
			}
		});
		
		$('.page-header, .search_container').show();
		$('#overview .wrapper').fadeIn();

		return {};
	};
});
