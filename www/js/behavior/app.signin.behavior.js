$(function(){
	
	window.App.view.signin.behavior = function() {
		var app = window.App;
		var $submitBtn = $('.loginBtnSubmit');
		
		$('.menu-button').hide();
		$('[data-role="header"]').hide();
		
		$submitBtn.click(function() {
			$.login({
				socialType : 'kickit',
				username : $('#login-form  [name="username"]').val(),
				password : $('#login-form  [name="password"]').val()
			}, function(s) {
				$.afterLogin(s);
			});
		});
		
		return {};	
	};
});