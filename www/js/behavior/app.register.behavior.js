$(function(){
	
	window.App.view.register.behavior = function() {
		var app = window.App;
		var $submitBtn = $('#register .btnSubmit');
		
		$('.menu-button').hide();
		$('[data-role="header"]').hide();
		
		$('.login-toggle-btn').click(function(){
			$('#login-form').attr('data-social-type', 'kickit');
			$('#login-form').slideToggle(function() {
				$('#login-form input')[0].focus();
			});
		});
		
		var $loginSubmitBtn = $('#register .loginBtnSubmit');
		$loginSubmitBtn.click(function() {
			$.login({
				socialType : 'kickit',
				username : $('#login-form  [name="username"]').val(),
				password : $('#login-form  [name="password"]').val()
			}, function(s) {
				$.afterLogin(s);
			});
		});
			
		$submitBtn.click(function(){
			$('#register-form').ajaxFormSubmit(function(data) {
				var obj = {};
				$.each(data, function(i, item) {
					obj[item.name] = item.value;
				});
				$.register(obj, function(res){
					if(res.success){
						obj.socialType = 'kickit';
						$.login(obj, function(s){
							$.afterLogin(s);
						});
					}else{
						alert(res.message);
					}
				});
			});
		});
		
		$('#register').attr('data-loaded', true);
		
		$('#register .choose-avatar').click(function(){
			$.avatarChooser(function(bytes){
				$('#register .featured_image').val(bytes);
			});
		});
		
		return {};	
	};
});