$(function() {
	window.App.view.contributor.behavior = function() {
		$.ajaxProxy({
			url : "users/" + $.getUrlVar('id') + ".json",
			callback : function(d) {
				var key = 'http://canikickitapp.com';
				var contributor = d;

				var html = "<div class='contributor-container'>";
				html += '<div style="max-height:300px;overflow:hidden;box-shadow:inset 0 0 10px #000;"><img src="' + (d.user.User.featured_image == null ? 'img/contributor_placeholder.jpg' : d.user.User.featured_image) + '" style="width:100%;min-height:100px;display:inline-block;" class="contributor-main-image" /></div>';
				html += "<div style='position: relative; left: 10px; top: 10px;'>";
				html += "<div class='name' >" + d.user.User.username + "</div>";
				html += "<div class='address' >" + d.user.User.address + "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='tel:" + d.user.User.phone + "' style='color:white; text-decoration:none;' >" + formatLocal("US", d.user.User.phone) + "</a></div>";
				html += "<br/></div>";
				html += "<div style='color:#000; background:#00CFF4; text-indent:10px;'><span style='font-size:12px;'>" + (d.user.User.website ? ("Website: " + d.user.User.website) : "") + "</span><br/>";
				html += "<div style='text-align:center; background:#000;padding-top:15px;padding-bottom:15px;'>";
				html += "<a class='view-specials-button' href='#' data-role='button' data-mini='true' data-theme='a'  data-inline='true'  >" + window.App.view.contributor.labels.barSpecialsHeader + "</a>";
				html += "<a href='javascript:void(0);' class='driving-directions' data-id='" + d.user.User.id + "' data-mini='true' data-theme='a'   data-inline='true'  data-role='button'>" + window.App.view.contributor.labels.drivingDirectionsButonLabel + "</a>";
				html += "<a class='view-menu-button' href='#' data-role='button'  data-id='" + d.user.User.id + "' data-mini='true' data-theme='a' data-inline='true' >MENU</a>";
				html += "</div>";
				html += "</div>";
				html += "<div style='display:none;' class='bar-specials-container'><br/><div style='padding-left:10px; padding-bottom:10px; font-size:16px; color:#f0ff2c;'>" + window.App.view.contributor.labels.barSpecialsHeader + "</div>";

				$.each(d.user.Specials, function(i, item) {
					html += '<table style="width:100%;" >';
					html += '<tr>';
					html += '<td style="vertical-align:top;text-align:left;width:120px;"><img src="' + item.featured_image + '" style="width:100px;display:inline-block;margin:5px;margin-top:0px;margin-bottom:0px;vertical-align:top;box-shadow:inset 0 0 10px #000;" /></td>';
					html += '<td style="vertical-align:top;text-align:left;">';
					html += '<span style="text-transform: uppercase; color: white; font-size: 16px;">' + item.title + '</span>';
					if (item.terms) {
						html += '<br/><span style="font-size:13px;">' + item.terms + '</span>';
					}
					html += '<br/>';
					html += '<tr>';
					html += '<td colspan="2">';
					html += "<div class='purchase-special-button-row' data-id='" + item.id + "' style='text-align:left;'>";
					html += "<div style='display: inline-block; background: none repeat scroll 0% 0% transparent; border-radius: 5px; border: 2px solid rgb(240, 255, 44); text-align: center; width: 115px; padding: 8px; font-size: 11px; color: rgb(240, 255, 44); margin-left: 10px; margin-top: 10px; '>" + window.App.view.contributor.labels.purchaseSpecialsHeader + "</div>";
					html += "<div style='margin-left:20px;display: inline-block; color: rgb(240, 255, 44);font-size:15px;' ><span style='color:white;'>EARN:</span> <img src='img/ticket-dark.png' style='vertical-align:middle;'/> x 5</div>";
					html += '</div>';
					html += '</td>';
					html += '</tr>';
					html += '</table>';
				});
				html += "</div>";
				html += "</div></div>";
				html += "<div class='slider'></div>";

				$('#contributor [data-role="content"]').html(html);

				if (d.user.Specials.length == 0) {
					$('.view-specials-button').hide();
				}
				
				if(d.user.User.menu == null){
					$('.view-menu-button').hide();
				}

				$('#contributor [data-role="content"]').trigger('create');
				$('.driving-directions').click(function() {
					var id = $(this).attr('data-id');
					$.getPosition(function(currentPosition) {
						
						$.ajaxProxy({
							url : "users/" + id + ".json",
							callback : function(d) {
								var contributor = d.user.User;
								var start = new google.maps.LatLng(currentPosition.latitude, currentPosition.longitude);
								var user = d.user.User;
								var end = user.address + ' ' + user.city + ' ' + user.state + ' ' + user.zip;
								var lat = user.latitude;
								var lng = user.longitude;
								var latLng = new google.maps.LatLng(lat, lng);
								console.log(latLng);
								
								var request = {
									origin : start,
									destination : latLng,
									travelMode : google.maps.TravelMode.DRIVING
								};
								
								var directionsService = new google.maps.DirectionsService();
								directionsService.route(request, function(response, status) {
									
									var leg = response.routes[0].legs[0];
									var steps = leg.steps;
									var html = "<div id='driving_map' style='height:150px;width:100%;'></div><br/>";
									html += "<div style='font-size:20px; text-align:center; text-transform:uppercase; color:#f0ff2c;'>" + leg.distance.text + " / " + leg.duration.text + "</div><br/>";
									html += "<ol>";
									$.each(steps, function(i) {
										html += "<li style='font-weight:normal !important;color:white; font-size:15px; padding:10px;'>" + this.instructions + "</li>";
									});
									html += "</ol>";
									alert("<div style='text-align:left; max-height:" + ($(window).height() - 75) + "px; overflow:auto;'>" + html + "</div>");

									var directionsDisplay = new google.maps.DirectionsRenderer();
									var mapOptions = {
										zoom : 12,
										center : start,
										mapTypeId : google.maps.MapTypeId.ROADMAP,
										panControl : false,
										zoomControl : false,
										mapTypeControl : false,
										scaleControl : false,
										streetViewControl : false,
										overviewMapControl : false
									};
									var map = new google.maps.Map($('#driving_map')[0], mapOptions);
									directionsDisplay.setMap(map);
									directionsDisplay.setDirections(response);
								});
							}
						});
					});
				});

				function processSpecial(id) {
					
					function barcodeAccept(id) {
						$.ajaxProxy({
							url : "specials/purchaseSpecial/" + id,
							type : 'post',
							data : {
								contributor_id : $.getUrlVar('id'),
								userId : window.App.user.id
							},
							callback : function(d) {
								var hashtag = window.App.view.socialmedia.labels.defaultSharingHashtag;
								window.App.user = d.user.User;
								//window.App.user.isFirstSpecialSubmit = true;

								$('[data-role="header"],.menu-button').hide();

								$('[data-role="content"]').replaceWith($.template('purchase-special-success-overlay', {
									name : contributor.user.User.username,
									numberTickets:d.numberTickets, 
									isFirst : window.App.user.isFirstSpecialSubmit,
									hashtag:hashtag
								}));

								$('.social-sharing-overlay').css({
									'height' : $(window).height()
								});

								$('.social-sharing-overlay a').button();

								$('.sharing-ct').click(function() {
									window.App.loadView('socialmedia');
									$('.close').trigger('click');
								});

								for (var i in window.App.user) {
									$('#force-profile-form [name="' + i + '"]').val(window.App.user[i]);
								}

								function validateAndSave(callback) {
									var error = false;
									$('.error-border').removeClass('error-border');
									if (window.App.user.isFirstSpecialSubmit) {
										for (var i in window.App.user) {
											var $el = $('#force-profile-form [name="' + i + '"]');
											if ($el.length > 0) {
												if ($el.val() == "") {
													error = true;
													$el.addClass('error-border');
												}
											}
										}
									}
									if (!error) {
										callback();
									}
								}

								function promtError(action) {
									if (window.App.user.isFirstSpecialSubmit) {
										$.confirm(window.App.view.global.labels.noProfileSaveError, function() {
											action();
										});
									} else {
										action();
									}
								}


								$('.save-profile').closest('.ui-btn').click(function() {
									validateAndSave(function() {
										window.App.loadView('profile');
									});
								});

								$('.go-to-profile').closest('.ui-btn').click(function() {
									promtError(function() {
										window.App.loadView('profile');
									});
								});
								$('.back-to-bar').closest('.ui-btn').click(function() {
									promtError(function() {
										window.App.loadView('contributor', $.getUrlVar('id'));
									});
								});
							}
						});
					}

					if ( typeof cordova != 'undefined' && cordova.plugins) {
						cordova.plugins.barcodeScanner.scan(function(result) {
							if (result.text == key) {
								barcodeAccept(id);
							} else {
								$.notify(window.App.view.contributor.labels.invalidQRCodeError);
							}
						}, function(error) {
							$.notify(window.App.view.contributor.labels.invalidQRCodeError);
						});
					} else {
						barcodeAccept(id);
					}
				}

				var picture = null;
				$('.purchase-special-button-row').click(function() {
					var id = $(this).attr('data-id');
					
					if (d.user.User.validate_location == 1) {
						$.getPosition(function(currentPosition) {
							
							
							
							var address = d.user.User.address + ' ' + d.user.User.city + ' ' + d.user.User.state + ' ' + d.user.User.zip;
							var geocoder = new google.maps.Geocoder();
							geocoder.geocode({
								address : address
							}, function(locResult) {
								var lat = locResult[0].geometry.location.lat();
								var lng = locResult[0].geometry.location.lng();
								var latLng = new google.maps.LatLng(lat, lng);
								
								
								
								var myLatLn = new google.maps.LatLng(currentPosition.latitude, currentPosition.longitude);
								console.log(latLng);
								console.log(myLatLn);
								if ($.isInRange(latLng, myLatLn)) {
									processSpecial(id);
								} else {
									$.notify(window.App.view.contributor.labels.specialLocationError);
								}
							});
						});
					} else {
						processSpecial(id);
					}
				});

				$('.view-specials-button').click(function() {
					$('.bar-specials-container').slideToggle(function(){
						$('#contributor [data-role="content"]').animate({
							scrollTop: $('.contributor-main-image').height() - 50
						}, 1000, null, alignBanner);
					});
				});
				
				$('.bar-specials-container img').click(function(){
					alert("<img src='" + $(this).attr('src') + "' />");
				});

				$('.view-menu-button').click(function() {
					var id = $(this).attr('data-id');
					$.viewContibutorMenu(id);
				});

				$('.slider').bannerSlider();

				function resize() {
					$('.contributor-image').css('height', $(window).height() * .50);
				}
				
				$(window).resize(resize);
				resize();
			}
		});
	};
	return {};
});

