function IG(clientId, clientSecert) {
	var returnURL = "http://canikickitapp.com/system/instagram.oauth.html";
	var url = "https://api.instagram.com/oauth/authorize/?scope=likes&client_id=" + clientId + "&redirect_uri=" + returnURL + "&response_type=token";

	var tokenName = "INSTAGRAM_TOKEN";
	var loginWindow = null;
	var processed = false;

	function _callback(val, obj) {
		if (appliedCallback != null) {
			appliedCallback(val, obj);
			processed = true;
		}
	}

	return {
		post : function(mediaId, callback) {
			Pace.track(function() {
				var options = {};
				options.data = {
					"access_token" : window[tokenName]
				};
				options.url = "https://api.instagram.com/v1/media/" + mediaId + "/likes";
				
				$.proxyPost(options, function(data) {
					callback(true);
				});
			});
		},
		login : function(callback) {
			processed = false;
			appliedCallback = callback;

			loginWindow = window.open(url, '_blank', 'location=no');

			function loadStop(event) {
				if (event.url.indexOf(returnURL) > -1) {
					var token = event.url.split("=")[1];
					if (token != null) {
						processed = true;
						window[tokenName] = token;
						_callback(true);
						loginWindow.close();
					}
				}
			}

			function loadExit() {
				loginWindow.removeEventListener('loadstop', loadStop);
				loginWindow.removeEventListener('exit', loadExit);
				loginWindow = null;
				if (!processed) {
					_callback(false);
				}
			}


			loginWindow.addEventListener("loadstop", loadStop);
			loginWindow.addEventListener('exit', loadExit);
		},
		logout : function(callback) {
			window[tokenName] = null;
			callback(true);
		},
		getLoginStatus : function(callback) {
			callback(window[tokenName] != null);
		}
	};
}

