(function($) {
	$.fn.extend({
		establishmentBrowser : function(d) {
			return this.each(function() {
				var $this = $(this);
				var html = '<div class="estiblisment-details-container">';
				var length = d.establishments.length;
				for (var i = 0; i < length; i++) {
					var est = d.establishments[i];
					html += '<div class="estiblisment-container" data-id="' + est.User.id + '" style="background-color:#333;">';
					html += '<table><tr>';
					var logo = (est.User.kick_it_at_location == '1') ? 'kiki-king-logo.png' : 'king-white.png';
					html += '<td style="width:40px;"><div class="number" style="background-image: url(img/' + logo + ');" ></div></td>';
					html += '<td ><div class="heading">' + est.User.username + '<br/>';
					html += '<div>' + est.User.address + '</div></div></td></tr></table>';
					html += '</div>';
				}
				html += '</div>';
				$this.html(html);
				$this.find('.estiblisment-container').click(function() {
					var id = $(this).attr('data-id');
					window.App.loadView('contributor', id);
				});
			});
		}
	});
})(jQuery);