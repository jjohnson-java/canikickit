(function($) {
	$.fn.extend({
		menuOptions : function() {
			return this.each(function() {
				var $this = $(this);	
				var naveLabels = window.App.view.global.labels;
				$this.html(
					'<a href="javascript:window.App.loadPreviousView();" data-key="back" data-role="button" data-theme="b" data-icon="back" >' + naveLabels.backButtonLabel + '</a>' + 
		            '<div style="border-top:1px solid #333;border-bottom:1px solid #555;"></div>' + 
		            '<a href="javascript:window.App.loadView(\'profile\');" data-key="profile" data-role="button" data-theme="b" data-icon="gear" >' + naveLabels.profileButtonLabel + '</a>' + 
		            '<a href="javascript:window.App.loadView(\'overview\');" data-key="overview" data-role="button" data-theme="b" data-icon="check" >' + naveLabels.mapsButtonLabel + '</a>' + 
		            '<a href="javascript:window.App.loadView(\'socialmedia\');" data-key="challanges" data-role="button" data-theme="b" data-icon="beer" >' + naveLabels.socialMediaButtonLabel + '</a>' + 
		            '<a href="javascript:window.App.loadView(\'trips\');" data-key="rewards" data-role="button" data-theme="b" data-icon="star" >' + naveLabels.prizesButtonLabel + '</a>' + 
		            '<a href="javascript:window.App.loadView(\'faq\');" data-key="faq" data-role="button" data-theme="b" data-icon="question-circle">' + naveLabels.faqButtonLabel + '</a>' + 
		            '<a href="javascript:window.App.loadView(\'about\');" data-key="about" data-role="button" data-theme="b" data-icon="info-circle">' + naveLabels.aboutButtonLabel + '</a>' + 
		            '<div style="border-top:1px solid #333;border-bottom:1px solid #555;"></div>' + 
		            '<a href="javascript:window.App.loadView(\'settings\');" data-key="settings" data-role="button" data-theme="b" data-icon="gear" >' + naveLabels.settingsButtonLabel + '</a>' + 
		            '<a href="javascript:window.App.showTips();" data-key="tips" data-role="button" data-theme="b" data-icon="back" >' + naveLabels.tipsButtonLabel + '</a>' + 
		            '<a href="javascript:$.logout();" data-role="button" data-key="switch" data-theme="b" data-icon="user">' + naveLabels.switchUserButtonLabel + '</a><br/><br/>'
		        );
				$this.trigger('create');
			});
		}
	});
})(jQuery);