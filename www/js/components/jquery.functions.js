(function($) {
	$.extend({
		loadContributors:function(callback){
			$.ajaxProxy({
				url : "contributors/activeContributors/",
				type : 'GET',
				data : {},
				callback : function(data){
					window.App.contributors = data;
					callback(window.App.contributors);
				}
			}, true);
		},
		getSpoofLocation: function(){
			return {coords:{
			 	latitude:39.636908,
			 	longitude:-104.904456
			}};
		},
		termsAndConditions: function(){
			var user = window.App.user;
			if(user.seen_terms_and_conditions == '0'){
				$.overlay('terms-and-conditions', {}, function() {});
				var terms = $.systetmVarByKey('terms_conditions');
				$('#terms-conditions-container').html('<label>' + window.App.view.register.labels.termsHeading + '</label><p>' + terms['SystemVariable']['value'] + '</p>');
				$('.close').hide();
				$("#terms").click(function(){
					if($(this).is(":checked")){
						user.seen_terms_and_conditions = '1';
						$.ajaxProxy({
							url : "users/" + user.id + ".json",
							type : 'POST',
							data : {id:user.id, seen_terms_and_conditions:user.seen_terms_and_conditions},
							callback : function(data){
								$('.close').trigger('click');
							}
						}, true);
					};
				});
			}
		},
		afterLogin:function(d){
			var app = window.App;
			app.transitionType = d.user.User.transition_type;
			app.systemVariables = d.systemVariables;
			app.user = d.user.User;
			
			$.termsAndConditions();
			
			$.geoLocation();
			
			setInterval(function(){
				window.App.notifyContributorInRange();
			}, 30000);
				
			app.loadView('profile');
		},
		prizeOverlay:function(item){
			function display(winner){
				$.overlay('trips-overlay', {
					item : item,
					winner:winner
				}, function() {});
			}
			if(item.winner_user_id){
				$.ajaxProxy({
					url : "users/" + item.winner_user_id + ".json",
					callback : function(winner) {
						display(winner);
					}
				});	
			}else{
				display(null);
			}
		},
		avatarChooser:function(callback){
			$.ajaxProxy({
				url : "avatars.json",
				type : 'GET',
				data : {},
				callback : function(response) {
					alert($.template("avatar-chooser", {avatars:response.rows}));
					if(navigator && navigator.camera){
						$('.avatar-action-btn').button();
						$('.avatar-action-btn').closest('.ui-btn').click(function(){
							var $this = $(this).find('.avatar-action-btn');
							navigator.camera.getPicture(function(successData){
    							callback("data:image/jpeg;base64," + successData);
    							$('.close').trigger('click');
							}, function(errorData){
								//$.notify('There was an error getting your picture.');
							}, {
								quality: 80,
    							destinationType: Camera.DestinationType.DATA_URL,
    							sourceType : $this.hasClass('take-your-own-photo') ? Camera.PictureSourceType.CAMERA : Camera.PictureSourceType.PHOTOLIBRARY,
								allowEdit : true,
								encodingType: Camera.EncodingType.JPEG,
								targetWidth: 100,
								targetHeight: 100,
								saveToPhotoAlbum: true 
							});
						});
					}else{
						$('.avatar-action-btn').hide();
					}
					
					$('.avatar-chooser .profile-image').click(function(){
						callback($(this).attr('data-image'));
						$('.close').trigger('click');
					});
				}
			});
		},
		viewContibutorMenu : function(id) {
			alert('<div style="height:500px;"><iframe src="http://docs.google.com/viewer?url=' + BASE_FULL_URL + 'users/getMenu/' + id + '&embedded=true" width="100%" height="500" style="border: none;"></iframe></div>');
			$('.close').next().css('top', 75);
		},
		getPosition : function(callback) {
			$.ajaxProxy({
				url : "users/getCurrentLocation/" + window.App.user.id,
				type : 'GET',
				data : {},
				callback : function(d) {
					callback(d);
				}
			});
		},
		mapOverlay : function() {
			if ($('.map-overlay').length > 0) {
				$('body').plainOverlay('show');
			} else {

				function build() {
					var d = window.App.mapData;
					alert("<div class='map-overlay' ></div>");
					$('.map-overlay').css({
						width : '100%',
						height : $(window).height() - $('.close').height()
					});
					var map = new google.maps.Map($('.map-overlay')[0], {
						zoom : 10,
						mapTypeId : google.maps.MapTypeId.ROADMAP,
						center : new google.maps.LatLng(39.7391667, -104.9841667),
						//styles : mapStyle,
						disableDefaultUI: true
					});
					
					/*new CustomMarker(new google.maps.LatLng(39.7391667, -104.9841667), map,{
						url:BASE_FULL_URL + 'timthumb.php?w=55&h=55&src=' + BASE_FULL_URL + 'featured_image/get/User/' + window.App.user.id,
						title:window.App.user.username.toUpperCase()
					});*/

					$('.map-overlay').contibutorMap(d, map);
				}

				var lat = window.App.currentPositionSource.coords.latitude;
				var lng = window.App.currentPositionSource.coords.longitude;
				var location = lat + "," + lng;
				$.ajaxProxy({
					url : "contributors/allOfThem",
					type : 'POST',
					data : {
						location : location,
						keyword : ""
					},
					callback : function(d) {
						window.App.mapData = d;
						build();
					}
				});
			}

		},
		proxyPost : function(options, callback) {
			$.ajaxProxy({
				url : "proxy_post/post",
				type : 'GET',
				dataType:'json',
				data : {
					options : JSON.stringify(options)
				},
				callback : function(response) {
					callback(response);
				}
			});
		},
		dayOfYear : function(now) {
			var start = new Date(now.getFullYear(), 0, 0);
			var diff = now - start;
			var oneDay = 1000 * 60 * 60 * 24;
			var day = Math.floor(diff / oneDay);
			return day;
		},
		isInRange : function(latLng1, latLng2) {
			if (latLng1 && latLng2) {
				var SameThreshold = 100;
				return (google.maps.geometry.spherical.computeDistanceBetween(latLng1, latLng2) < SameThreshold);
			} else {
				return false;
			}
		},
		notify : function(message) {
			try {
				if (navigator && navigator.notification) {
					navigator.notification.alert(message, null, "CIKI MESSAGE");
				} else {
					alert(message);
				}
			} catch(e) {
				alert(message);
			}
		},
		promptNotify : function(message, title, callback) {
			$.prompt(message, callback);
		},

		postToTwitter : function(message) {
			var hashtag = window.App.view.socialmedia.labels.defaultSharingHashtag;
				$.genericPost(message, function(defaultMessage, fileBytes){
					Pace.track(function() {
						function afterImageUpload(url){
							if(window.plugins){
								window.plugins.socialsharing.shareViaTwitter(
									defaultMessage + ' ' + hashtag, 
									'http://canikickitapp.com/system/img/static_upload/' + url, 
									null,
									function(result) {
										if(result){
											$.postSuccessMessage();
										}
									}, function(errormsg) {
										$.notify(errormsg);	
									}
								);
							}else{
								$.notify('Sharing plugin not installed');	
							}
						}
						if(fileBytes != null){
							$.ajaxProxy({
								url : "featured_image/uploadStatic",
								type : 'POST',
								data:{fileBytes:fileBytes},
								callback : function(r){
									afterImageUpload(r);
								}
							});
						}else{
							afterImageUpload('default_icon.png');
						}
					});
				}, true);
			/*	$.genericPost(message, function(defaultMessage){
					
					
					
					Pace.track(function() {
						TWITTER.post(defaultMessage, hashtag, function() {
							$.postSuccessMessage(defaultMessage, null);
						});
					});
				}, true);*/
				
			//});
		},
		twitterAction : function(callback) {
			$.checkTwitterStatus(function(connected) {
				if (!connected) {
					$.twitterLogin(function() {
						callback();
					});
				} else {
					callback();
				}
			});
		},
		twitterLogoff : function(callback) {
			TWITTER.logout(callback);
		},
		twitterLogin : function(callback) {
			TWITTER.login(function(connected, response) {
				callback(connected, response);
			});
		},
		checkTwitterStatus : function(callback) {
			TWITTER.getLoginStatus(callback);
		},
		postToInstagram : function(message) {
			var mediaId = $.systetmVarByKey("instagram_media_id")['SystemVariable']['value'];
			$.instagramAction(function() {
				Instagram.isInstalled(function (err, installed) {
					if (installed) {
				    	$.genericPost(message, function(defaultMessage, fileBytes){
							Pace.track(function() {
								Instagram.share(fileBytes, defaultMessage, function (err) {
								    if (err) {
								        $.notify(err);
								    } else {
								        $.postSuccessMessage(defaultMessage, fileBytes);
								    }
								});
							});
						}, true);
					} else {
		    			$.notify("You must have the Instagram app installed to complete this action.");
	    			}
				});
    		});
		},
		instagramLogoff : function(callback) {
			IG.logout(callback);
		},
		instagramAction : function(action) {
			$.checkInstagramStatus(function(connected) {
				if (!connected) {
					$.instagramLogin(function() {
						action();
					});
				} else {
					action();
				}
			});
		},
		instagramLogin : function(callback) {
			IG.login(function(response) {
				callback(response);
			});
		},
		checkInstagramStatus : function(callback) {
			IG.getLoginStatus(function(response) {
				callback(response);
			});
		},
		genericPost:function(defaultMessage, callback, allowImage){
			var hashtag = window.App.view.socialmedia.labels.defaultSharingHashtag;
			var instructions = window.App.view.socialmedia.labels.sharingInstructions.replace('[sharing-hashtag]', hashtag);
			
			
			$('[data-role="content"]').html($.template('generic-post', {instructions:instructions, message:defaultMessage, allowImage:allowImage}), "");
			$('.post-submit, .post-cancel').button();
			
			
			
			var photoBytes = null;
			$('.post-submit').closest('.ui-btn').click(function(){
				var user = window.App.user;
				var message = defaultMessage;
				var hasMesage = message != null && message != "";
				var id = user.id;
				
				if(hasMesage){
					user.number_of_tickets = parseInt(user.number_of_tickets) + 5;
				}
				
				$.ajaxProxy({
					url : "users/" + id + ".json",
					type : 'POST',
					data:user,
					callback : function(r){}
				});
				
				callback(message, photoBytes);
				
				window.App.loadView('socialmedia');
			});
			$('.upload-image').button();
			if(navigator && navigator.camera){
				
				$('.upload-image').closest('.ui-btn').click(function(){
					var isDevice = $(this).find('a').hasClass('upload-image-device');
					navigator.camera.getPicture(function(successData){
						photoBytes = ("data:image/jpeg;base64," + successData);
						$('.thumbnailholder').html("<img src='" + photoBytes + "' style='border:3px solid #2EB9E3;' />");
					}, function(errorData){
						//$.notify('There was an error getting your picture.');
					}, {
						quality: 80,
						destinationType: Camera.DestinationType.DATA_URL,
						sourceType : isDevice ? Camera.PictureSourceType.PHOTOLIBRARY : Camera.PictureSourceType.CAMERA,
						allowEdit : true,
						encodingType: Camera.EncodingType.JPEG,
						targetWidth: 240,
						targetHeight: 380,
						saveToPhotoAlbum: !isDevice 
					});
				});
			}else{
				$('.upload-image').hide();
			}
			
			$('.post-cancel').closest('.ui-btn').click(function(){
				window.App.loadView('socialmedia');
			});
			
			$('.upload-image').each(function(){
				$(this).closest('.ui-btn').addClass('btn-dark');
				$(this).closest('.ui-btn').style('padding', '5px');
			});
			$('.post-cancel').closest('.ui-btn').addClass('btn-med-dark');
			
		},
		postSuccessMessage:function(){
			var app = window.App;
			var user = app.user;
			var hashtag = window.App.view.socialmedia.labels.defaultSharingHashtag;
			var html = '<div class="sharing_success_message"><h1 style="font-size:35px !important;color:#E5F32D;">SUCCESS</h1>';
			html += window.App.view.socialmedia.labels.sharingSuccessMessage + "<br/>";
			html += window.App.view.socialmedia.labels.sharingInstructions.replace('[sharing-hashtag]', hashtag);
			html += "<br/><br/>";
			html += "<a href='#' class='share' style='font-size:18px;vertical-align:middle;color:white;text-transform:uppercase;text-decoration:none;'  >SHARE MORE</a>";
			html += "<a href='#' class='profile' style='font-size:18px;vertical-align:middle;color:white;text-transform:uppercase;text-decoration:none;'  >PROFILE</a></div>";

			alert(html, "");
			
			$('.sharing_success_message a').button();
			
			$('.sharing_success_message a').closest('.ui-btn').click(function(){
				if($(this).find('a').hasClass('share')){
					window.App.loadView('socialmedia');
				}else{
					window.App.loadView('profile');
				}
				$('.close').trigger('click');
			});
			
			$.ajaxProxy({
				url : "/users/enterWeeklyPromotion",
				type : 'get',
				data : {userId:user.id},
				callback : function(data) {
					if(data.success){
						window.App.user = data.user.User;
						$.saveLoginDetails(window.App.user);
					}else{
						$.notify(data.message);
					}
				}
			});
		},
		postToFaceBook : function(message) {
			var feedId = $.systetmVarByKey("facebook_feed_id")['SystemVariable']['value'];
			var hashtag = window.App.view.socialmedia.labels.defaultSharingHashtag;
			$.genericPost(message, function(defaultMessage, fileBytes){
				Pace.track(function() {
					function afterImageUpload(url){
							if(window.plugins){
								window.plugins.socialsharing.shareViaFacebook(
									defaultMessage + ' ' + hashtag, 
									'http://canikickitapp.com/system/img/static_upload/' + url, 
									null,
									function(result) {
										if(result){
											$.postSuccessMessage();
										}
									}, function(errormsg) {
										$.notify(errormsg);	
									}
								);
							}else{
								$.notify('Sharing plugin not installed');	
							}
					}
					if(fileBytes != null){
						$.ajaxProxy({
							url : "featured_image/uploadStatic",
							type : 'POST',
							data:{fileBytes:fileBytes},
							callback : function(r){
								afterImageUpload(r);
							}
						});
					}else{
						afterImageUpload('default_icon.png');
					}
				});
			}, true);
		},
		facebookLogoff : function(callback) {
			openFB.logout(callback);
		},
		facebookLogin : function(callback) {
			Pace.track(function() {
				openFB.login(function(r) {
					console.log(r);
					callback(r);
				}, {
					scope : 'public_profile, email'
				});
			});
		},
		facebookAction : function(action) {
			$.checkFacebookStatus(function(connected) {
				if (!connected) {
					$.facebookLogin(function() {
						action();
					});
				} else {
					action();
				}
			});
		},
		checkFacebookStatus : function(callback) {
			openFB.getLoginStatus(function(loginStatus) {
				callback(loginStatus.status == 'connected');
			});
		},
		googleAction : function(action) {
			$.checkGoogleStatus(function(status) {
				if (status) {
					action();
				} else {
					$.googleLogin(action, false);
				}
			});
		},
		googleLogoff : function(callback) {
			GOOGLE.logout(callback);
		},
		googleLogin : function(callback, silent) {
			GOOGLE.login(function(response, obj) {
				callback(response, obj);
			});
		},
		checkGoogleStatus : function(callback) {
			GOOGLE.getLoginStatus(function(response) {
				callback(response);
			});
		},
		postToGoogle : function(message) {
			$.googleAction(function() {
				GOOGLE.post(message, function() {

				});
			});
		},
		overlay : function(tpl, data, fun) {
			$('body').plainOverlay({
				progress : function() {
					return $("<div class='close'></div><div style='width:100%;height:" + ($(window).height() - 75) + "px;' ><div>" + $.template(tpl, data) + "</div></div>");
				}
			});
			$('body').plainOverlay('show');

			$('.close').click(function() {
				$('body').plainOverlay('hide');
			});
			$('.close').next().addClass('overlay');
			
			fun();
		},
		mysqlToFormat : function(timestamp) {
			return $.formatDate($.mysqlToDate(timestamp));
		},
		mysqlToDate : function(timestamp) {
			if (timestamp) {
				var t = timestamp.split(/[- :]/);
				return new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
			} else {
				return new Date();
			}
		},
		formatDate : function(date) {
			var d = date;
			var curr_date = d.getDate();
			var curr_month = d.getMonth() + 1;
			var curr_year = d.getFullYear();
			return (curr_month + "/" + curr_date + "/" + curr_year);
		},
		focusAnything : function(selector) {
			setTimeout(function() {
				$(selector).find("input, select, textarea").each(function(i) {
					if (i == 0)
						$(this)[0].focus();
				});
			}, 2000);
		},
		systetmVarByKey : function(key) {
			var returnItem = null;
			$.each(window.App.systemVariables, function(i, item) {
				if (item.SystemVariable.key == key) {
					returnItem = item;
				}
			});
			return returnItem;
		},
		getUrlVarsByString : function(str) {
			var vars = [],
			    hash;
			var hashes = str.slice(str.indexOf('?') + 1).split('&');
			for (var i = 0; i < hashes.length; i++) {
				hash = hashes[i].split('=');
				vars.push(hash[0]);
				vars[hash[0]] = hash[1];
			}
			return vars;
		},
		getUrlVars : function() {
			return $.getUrlVarsByString(window.location.href);
		},
		getUrlVar : function(name) {
			return $.getUrlVars()[name];
		},
		getOrDefault : function(item, de) {
			if ( typeof item == 'undefined' || item == null || !item) {
				return de;
			}
			return item;
		},
		getOrNull : function(item) {
			return $.getOrDefault(item, null);
		},
		alert : function(s) {
			alert(s);
		},
		confirm : function(s, callback) {
			if(navigator.notification){
				navigator.notification.confirm(s, function(is){
					if(is == 1){
						callback(true);	
					}
				});
			}else{
				alert("<div>" + s + "<br/><br/><a href='javascript:void(0);' data-role='button' style='text-transform:uppercase; color:white; text-decoration:none;' class='confirm_btn' >YES PLEASE</a></div>");
				$('.confirm_btn').button();
				$('.confirm_btn').closest('.ui-btn').click(function() {
					$('.close').trigger('click');
					callback(true);
				});
			}
		},
		prompt : function(s, callback) {
			alert("<div>" + s + "<br/><input type='text'class='prompt_input'  /><br/><br/><a href='javascript:void(0);' data-role='button' style='text-transform:uppercase; color:white; text-decoration:none;' class='confirm_btn' >YES PLEASE</a></div>");
			$('.confirm_btn').button().click(function() {
				callback($('.prompt_input').val());
				$('.close').trigger('click');
			});
		},
		logout : function(showMessage) {
			function exec(d) {
				if (d) {
					$.ajaxProxy({
						url : "users/logout",
						type : 'POST',
						callback : function(d) {
							$('.close, .overlay').remove();
							window.localStorage.setItem('loginDetails', null);
							window.App.loadView('login');
						}
					});
				}
			}

			//if (showMessage) {
			$.confirm(window.App.view.login.labels.logoutConfirm, exec);
			//} else {
			//	exec(true);
			//}
		},
		login : function(data, callback) {
			$.ajaxProxy({
				url : "users/login",
				type : 'POST',
				data : data,
				callback:function(d){
					console.log(d);
					if (d.success) {
						callback(d);
						$.saveLoginDetails(d.user['User']);
						
					} else {
						$.notify(d.error);
						window.App.loadView('login');
					}
				}
			});
		},
		loadLoginDetails : function() {
			return $.parseJSON(window.localStorage.getItem('loginDetails'));
		},
		saveLoginDetails : function(details) {
			var obj = {
				username : details.username,
				password : details.password
			};
			window.App.user = details;
			window.localStorage.setItem('loginDetails', JSON.stringify(obj));
		},
		register : function(data, callback) {
			$.ajaxProxy({
				url : "users/add",
				type : 'POST',
				data : data,
				callback : function(d) {
					if (d.success) {
						callback(d);
					} else {
						$.notify(d.message);
					}
				}
			});
		},
		forgotPassword : function(data, callback) {
			$.ajaxProxy({
				url : "users/passwordReset",
				type : 'POST',
				data : data,
				callback : function(d) {
					if (d.success) {
						callback(d);
					} else {
						$.notify(d.error);
					}
				}
			});
		},
		loadScript : function(script) {
			$("body").append('<script type="text/javascript" src="' + script + '"></script>');
		},
		arrayToMap : function(key, value, array) {
			var response = {};
			$.each(array, function(i, item) {
				var model = $.first(item);
				response[model[key]] = model[value];
			});
			return response;
		},
		first : function(obj) {
			for (key in obj)
			return obj[key];
		},
		truncate : function(str, length) {
			return str.length > length ? str.substring(0, length - 3) + '...' : str;
		},
		singular : function(s) {
			if (s) {
				var last = s.slice(-1);
				if (last.toLowerCase() == 's') {
					s = s.substring(0, s.length - 1);
				}
			}
			return s;
		},
		humanize : function(str) {
			str = str || "";
			return str.replace(/_/g, ' ').replace(/-/g, ' ').toUpperCase();
		},
		randomId : function() {
			var len = 5;
			var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
			var maxPos = $chars.length;
			var pwd = '';
			for ( i = 0; i < len; i++) {
				pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
			}
			return pwd;
		},
		randomString : function() {
			var len = 32;
			var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
			var maxPos = $chars.length;
			var pwd = '';
			for ( i = 0; i < len; i++) {
				pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
			}
			return pwd;
		},
		template : function(name, data) {
			return new EJS({
				url : "js/tpl/" + name + ".ejs"
			}).render(data);
		},
		mask : function() {
			if ($('.mask').length == 0) {
				$('<div class="mask" style="z-index:50000 !important;"></div>').appendTo('body');
				$.mobile.loading("show", {
					text : "",
					textVisible : false,
					theme : "b",
					html : ""
				});
			}
		},
		unmask : function() {
			$.mobile.loading("hide");
			$('.mask').fadeOut(function() {
				$(this).remove();
			});
		}
	});

	window.alert = function(s) {
		$.overlay('alert-overlay', {
			value : s
		}, function() {
		});
		$('.close').next().css('top', 75);
		setTimeout(function() {
			$('.overlay').css('height', $(window).height() - 75);
		}, 1500);
	};
})(jQuery);

Date.prototype.dayOfYear = function() {
	var j1 = new Date(this);
	j1.setMonth(0, 0);
	return Math.round((this - j1) / 8.64e7);
};

Array.prototype.merge = function(/* variable number of arrays */) {
	for (var i = 0; i < arguments.length; i++) {
		var array = arguments[i];
		for (var j = 0; j < array.length; j++) {
			if (this.indexOf(array[j]) === -1) {
				this.push(array[j]);
			}
		}
	}
	return this;
};

String.prototype.format = function() {
	var formatted = this;
	for (var arg in arguments ) {
		formatted = formatted.replace("{" + arg + "}", arguments[arg]);
	}
	return formatted;
};
/*
 *
 */


(function($) {    
  if ($.fn.style) {
    return;
  }

  // Escape regex chars with \
  var escape = function(text) {
    return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
  };

  // For those who need them (< IE 9), add support for CSS functions
  var isStyleFuncSupported = !!CSSStyleDeclaration.prototype.getPropertyValue;
  if (!isStyleFuncSupported) {
    CSSStyleDeclaration.prototype.getPropertyValue = function(a) {
      return this.getAttribute(a);
    };
    CSSStyleDeclaration.prototype.setProperty = function(styleName, value, priority) {
      this.setAttribute(styleName, value);
      var priority = typeof priority != 'undefined' ? priority : '';
      if (priority != '') {
        // Add priority manually
        var rule = new RegExp(escape(styleName) + '\\s*:\\s*' + escape(value) +
            '(\\s*;)?', 'gmi');
        this.cssText =
            this.cssText.replace(rule, styleName + ': ' + value + ' !' + priority + ';');
      }
    };
    CSSStyleDeclaration.prototype.removeProperty = function(a) {
      return this.removeAttribute(a);
    };
    CSSStyleDeclaration.prototype.getPropertyPriority = function(styleName) {
      var rule = new RegExp(escape(styleName) + '\\s*:\\s*[^\\s]*\\s*!important(\\s*;)?',
          'gmi');
      return rule.test(this.cssText) ? 'important' : '';
    }
  }

  // The style function
  $.fn.style = function(styleName, value, priority) {
    // DOM node
    var node = this.get(0);
    // Ensure we have a DOM node
    if (typeof node == 'undefined') {
      return this;
    }
    // CSSStyleDeclaration
    var style = this.get(0).style;
    // Getter/Setter
    if (typeof styleName != 'undefined') {
      if (typeof value != 'undefined') {
        // Set style property
        priority = typeof priority != 'undefined' ? priority : '';
        style.setProperty(styleName, value, priority);
        return this;
      } else {
        // Get style property
        return style.getPropertyValue(styleName);
      }
    } else {
      // Get CSSStyleDeclaration
      return style;
    }
  };
})(jQuery);