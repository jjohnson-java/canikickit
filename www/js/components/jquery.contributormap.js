(function($) {
	$.fn.extend({
		contibutorMap : function(data, map) {

			return this.each(function() {
				var $this = $(this);
				var currentMarker = null;

				function addMarker(lat, lng, icon, title){
					var latLng = new google.maps.LatLng(lat, lng);
					console.log(icon);
					return new google.maps.Marker({
				      position: latLng,
				      map: map,
				      animation: google.maps.Animation.DROP,
				      icon:icon,
				      title:title
				  	});
				}
				
				function getMarkerIcon(contributor){
					var icon = 'img/king-white.png';
					if(contributor.User.kick_it_at_location == '1'){
						icon = 'img/kiki-visited.png';
					}else if(contributor.User.showin_ui == '1'){
						icon = 'img/kiki-king-logo.png';
					}
					return icon;
				}
				function processC(index){
					//var infowindow = null;
					$.each(data.allContributors, function(i, item){
						var title = item.User.username.toUpperCase();
						var lat = item.User.latitude;
						var lng = item.User.longitude;
						
						var marker = addMarker(lat, lng, getMarkerIcon(item), title);
						marker.infowindow = null;
						
					  	google.maps.event.addListener(marker, 'click', function() {
							if(marker.infowindow != null){
								infowindow.close(); 
							}
							
							marker.infowindow = new google.maps.InfoWindow({
						      content: $.template('google-map-info-window', item)
						    });
						    
						    google.maps.event.addListener(marker.infowindow,'closeclick',function(){
							    marker.infowindow = null;
							});
						    
							marker.infowindow.open(map, marker);
							
							setTimeout(function(){
								$('.view-contributor-link').click(function(){
									window.App.loadView('contributor', item.User.id);
									$('.close').trigger('click');
								});
							}, 1500);
							
						});
					});
				}

				function addUsersMarker(){
					var lat = window.App.currentPosition.coords.latitude;
					var lng = window.App.currentPosition.coords.longitude;
					var latLng = new google.maps.LatLng(lat, lng);
					new CustomMarker(latLng, map,{
						url:BASE_FULL_URL + 'timthumb.php?w=55&h=55&src=' + BASE_FULL_URL + 'featured_image/get/User/' + window.App.user.id,
						title:window.App.user.username.toUpperCase()
					});
					
					map.setCenter(latLng);
				}
				if(window.App.currentPosition == null){
					navigator.geolocation.getCurrentPosition(function(currentPosition){
						window.App.currentPosition = currentPosition;
						addUsersMarker();
					}, function(e){
						
					}, {timeout: 20000, enableHighAccuracy: true, maximumAge: 75000});
				}else{
					addUsersMarker();
				}
			    processC(0);
			});
		}
	});
})(jQuery);


/*$.each(data.all, function(i, item){
					var title = this.name.toUpperCase();

					$.ajaxProxy({
						url : "contributors/getPlace/" + item.place_id,
						callback : function(d) {
							var latLng = new google.maps.LatLng(d.geometry.location.lat, d.geometry.location.lng);
							var marker = new google.maps.Marker({
								position : latLng,
								map : map,
								title:title,
								animation : google.maps.Animation.DROP,
								icon :'img/map-marker.png'
							});
							google.maps.event.addListener(marker, 'click', function() {
							   $($('.estiblishment-container')[i]).trigger('click');
							});
						}
					});
				});*/
