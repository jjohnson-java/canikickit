(function($) {
	$.fn.extend({
		awardsBrowser : function(promotoion) {
			return this.each(function(){
				var $this = $(this);
				var app = window.App;
				var user = app.user;
				$.ajaxProxy({
					url:'promotions/getCompleteionData/' + user.id + "/" + app.promotionId,
					type:'get',
					dataType:'json',
					callback:function(p){
						var html = '<div class="awards-browser-container">';
						html +='<table><tr>';
						html +='<td><span>' + p.num_contributors_complete + '</span><br/>CHALLENGES<br/>COMPLETE</td>';
						html +='<td><span>' + p.total_points + '</span><br/><span style="font-size:30px;">PTS</span></td>';
						html +='<td><span>' + (p.total_contributors - p.num_contributors_complete) + '</span><br/>CHALLENGES<br/>REMAINING</td>';
						html +='</tr></table>';
						html +='</div>';
						$this.html(html);
					}
				});
			});
		}
	});
})(jQuery);