(function($) {
	$.extend({
		dayOfYear: function(date){
		    var j1= new Date(date);
		    j1.setMonth(0, 0);
		    return Math.round((date-j1)/8.64e7);
		},
		isInRange : function(latLng1, latLng2){
			if(latLng1 && latLng2){
				var SameThreshold = 50;
				return (google.maps.geometry.spherical.computeDistanceBetween(latLng1,latLng2) < SameThreshold);
			}else{
				return false;
			}
		},
		notify:function(message){
			try{
				if(navigator && navigator.notification){
					navigator.notification.alert(message);
				}else{
					alert(message);
				}
			}catch(e){
				alert(message);
			}
		},
		promptNotify:function(message, title, callback){
			$.prompt(message, callback);;
		},
		
		promptUserShare : function(message) {
			if(window.plugins){
				confirm("Would you like to share on your page as well?", "", function(){
					window.plugins.socialsharing.share(message, null, null);
				});
			}
		},
		twitterAction:function(callback){
			$.checkTwitterStatus(function(connected){
				if(!connected){
					$.twitterLogin(function(){
						action();
					});
				}else{
					action();
				}
			});
		},
		twitterLogoff:function(callback){
			TWITTER.logout(callback);
		},
		twitterLogin:function(callback){
			TWITTER.login(function (response) {
			    callback(response);
			});
		},
		checkTwitterStatus:function(callback){
			callback(window.localStorage.getItem('TWITTER_TOKEN') != null);
		},
		instagramLogoff:function(callback){
			IG.logout(callback);
		},
		instagramAction:function(action){
			$.checkInstagramStatus(function(connected){
				if(!connected){
					$.instagramLogin(function(){
						action();
					});
				}else{
					action();
				}
			});
		},
		instagramLogin:function(callback){
			IG.login(function (response) {
			    callback(response);
			});
		},
		checkInstagramStatus:function(callback){
			IG.getLoginStatus(function(response){
				callback(response);
			});
		},
		facebookLogoff:function(callback){
			openFB.logout(callback);
		},
		facebookLogin:function(callback){
			openFB.login(callback, {scope:'publish_actions'});
		},
		facebookAction:function(action){
			$.checkFacebookStatus(function(connected){
				if(!connected){
					$.facebookLogin(function(){
						action();
					});
				}else{
					action();
				}
			});
		},
		checkFacebookStatus:function(callback){
			openFB.getLoginStatus(function(loginStatus) {
				callback(loginStatus.status == 'connected');
			});
		},
		googleAction:function(action){
			$.checkGoogleStatus(function(status){
				if(status){
					action();
				}else{
					$.googleLogin(action, false);
				}
			});
		},
		googleLogoff:function(callback){
			GOOGLE.logout(callback);
		},
		googleLogin:function(callback, silent){
			GOOGLE.login(function (response) {
			    callback(response);
			});
		},
		checkGoogleStatus:function (callback){
			GOOGLE.getLoginStatus(function(response){
				callback(response);
			});
		},
		postToSocialMedia : function(title, message, token, action) {
			function postToKickIt(){
				$.ajaxProxy({
					url : "social_media/" + action,
					type : 'POST',
					data : {
						title : title,
						message : message,
						token:token
						
					},
					callback : function(d) {
						$.notify(window.App.view.socialmedia.labels.sharingSuccessMessage);
					}
				});
			}
			if(window.plugins){
				window.plugins.socialsharing.share(message, null, null, postToKickIt);
			}else{
				postToKickIt();
			}
		},
		postToFaceBook : function(title, message) {
			$.facebookAction(function(){
				$.postToSocialMedia(title, message, window.sessionStorage['fbtoken'], "postToFaceBook");
			});
		},
		postToTwitter : function(title, message) {
			$.postToSocialMedia(title, message, window.localStorge.getItem("TWITTER_TOKEN"), "postToTwitter");
		},

		postToGoogle : function(title, message) {
			$.googleAction(function(){
				$.postToSocialMedia(title, message, window.localStorge.getItem("GOOGLE_TOKEN"), "postToGoogle");
			});
		},

		postToInstagram : function(title, message) {
			$.instagramAction(function(){
				$.postToSocialMedia(title, message, window.localStorge.getItem("INSTAGRAM_TOKEN"), "postToInstagram");
			});
		},
		overlay : function(tpl, data, fun) {
			$('body').plainOverlay({
				progress : function() {
					return $("<div class='close'></div><div style='width:100%;margin:auto;'>" + $.template(tpl, data) + "</div>");
				}
			});
			$('body').plainOverlay('show');

			$('.close').click(function() {
				$('body').plainOverlay('hide');
			});
			$('.close').next().addClass('overlay').css('height', $(window).height());
			$('.overlay').css('height', $('.overlay').parent().height() - 100);
			fun();

		},
		mysqlToFormat : function(timestamp) {
			return $.formatDate($.mysqlToDate(timestamp));
		},
		mysqlToDate : function(timestamp) {
			if(timestamp){
				var t = timestamp.split(/[- :]/);
				return new Date(t[0], t[1] - 1, t[2], t[3], t[4], t[5]);
			}else{
				return new Date();
			}
		},
		formatDate : function(date) {
			var d = date;
			var curr_date = d.getDate();
			var curr_month = d.getMonth() + 1;
			var curr_year = d.getFullYear();
			return (curr_month + "/" + curr_date + "/" + curr_year);
		},
		focusAnything : function(selector) {
			setTimeout(function() {
				$(selector).find("input, select, textarea").each(function(i) {
					if (i == 0)
						$(this)[0].focus();
				});
			}, 2000);
		},
		systetmVarByKey : function(key) {
			var returnItem = null;
			$.each(window.App.systemVariables, function(i, item) {
				if (item.SystemVariable.key == key) {
					returnItem = item;
				}
			});
			return returnItem;
		},
		getUrlVarsByString : function(str) {
			var vars = [],
			    hash;
			var hashes = str.slice(str.indexOf('?') + 1).split('&');
			for (var i = 0; i < hashes.length; i++) {
				hash = hashes[i].split('=');
				vars.push(hash[0]);
				vars[hash[0]] = hash[1];
			}
			return vars;
		},
		getUrlVars : function() {
			return $.getUrlVarsByString(window.location.href);
		},
		getUrlVar : function(name) {
			return $.getUrlVars()[name];
		},
		getOrDefault : function(item, de) {
			if ( typeof item == 'undefined' || item == null || !item) {
				return de;
			}
			return item;
		},
		getOrNull : function(item) {
			return $.getOrDefault(item, null);
		},
		alert : function(s) {
			alert(s);
		},
		confirm : function(s, callback) {
			alert("<div>" + s + "<br/><br/><a href='javascript:void(0);' data-role='button' style='text-transform:uppercase; color:white; text-decoration:none;' class='confirm_btn' >YES PLEASE</a></div>");
			$('.confirm_btn').button().click(function() {
				$('.close').trigger('click');
				callback(true);
			});
			
		},
		prompt : function(s, callback) {
			alert("<div>" + s + "<br/><input type='text'class='prompt_input'  /><br/><br/><a href='javascript:void(0);' data-role='button' style='text-transform:uppercase; color:white; text-decoration:none;' class='confirm_btn' >YES PLEASE</a></div>");
			$('.confirm_btn').button().click(function() {
				callback($('.prompt_input').val());
				$('.close').trigger('click');
			});
		},
		logout : function(showMessage) {
			function exec(d) {
				if (d) {
					$.ajaxProxy({
						url : "users/logout",
						type : 'POST',
						callback : function(d) {
							window.App.loadView('login');
						}
					});
				}
			}

			//if (showMessage) {
			$.confirm(window.App.view.login.labels.logoutConfirm, exec);
			//} else {
			//	exec(true);
			//}
		},
		watchGeoPosition : function() {
			if ( typeof window.App.geoWatchRunning == "undefined") {
				function set(position) {
					window.App.currentPosition = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
				}


				navigator.geolocation.getCurrentPosition(set);
				navigator.geolocation.watchPosition(set);

				window.App.geoWatchRunning = true;
			}
		},
		login : function(data, callback) {
			$.ajaxProxy({
				url : "users/login",
				type : 'POST',
				data : data,
				callback : function(d) {
					window.App.loadViewId = 'profile';
					if (d.success) {
						$.saveLoginDetails(d.user['User']);
						callback(d);
					} else {
						$.notify(d.error);
						window.App.loadView('login');
					}
				}
			});
		},
		loadLoginDetails : function() {
			return $.parseJSON(window.localStorage.getItem('loginDetails'));
		},
		saveLoginDetails : function(details) {
			var obj = {
				username : details.username,
				password : details.password
			};
			window.App.user = details;
			window.localStorage.setItem('loginDetails', JSON.stringify(obj));
		},
		register : function(data, callback) {
			$.ajaxProxy({
				url : "users/add",
				type : 'POST',
				data : data,
				callback : function(d) {
					if (d.success) {
						callback(d);
					} else {
						$.notify(d.error);
					}
				}
			});
		},
		forgotPassword : function(data, callback) {
			$.ajaxProxy({
				url : "users/passwordReset",
				type : 'POST',
				data : data,
				callback : function(d) {
					if (d.success) {
						callback(d);
					} else {
						$.notify(d.error);
					}
				}
			});
		},
		loadScript : function(script) {
			$("body").append('<script type="text/javascript" src="' + script + '"></script>');
		},
		arrayToMap : function(key, value, array) {
			var response = {};
			$.each(array, function(i, item) {
				var model = $.first(item);
				response[model[key]] = model[value];
			});
			return response;
		},
		first : function(obj) {
			for (key in obj)
			return obj[key];
		},
		truncate : function(str, length) {
			return str.length > length ? str.substring(0, length - 3) + '...' : str;
		},
		singular : function(s) {
			if (s) {
				var last = s.slice(-1);
				if (last.toLowerCase() == 's') {
					s = s.substring(0, s.length - 1);
				}
			}
			return s;
		},
		humanize : function(str) {
			str = str || "";
			return str.replace(/_/g, ' ').replace(/-/g, ' ').toUpperCase();
		},
		randomString : function() {
			var len = 32;
			var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
			var maxPos = $chars.length;
			var pwd = '';
			for ( i = 0; i < len; i++) {
				pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
			}
			return pwd;
		},
		template : function(name, data) {
			return new EJS({
				url : "js/tpl/" + name + ".ejs"
			}).render(data);
		},
		mask : function() {
			if ($('.mask').length == 0) {
				$('<div class="mask" style="z-index:50000 !important;"></div>').appendTo('body');
				$.mobile.loading("show", {
					text : "",
					textVisible : false,
					theme : "b",
					html : ""
				});
			}
		},
		unmask : function() {
			$.mobile.loading("hide");
			$('.mask').fadeOut(function() {
				$(this).remove();
			});
		}
	});

	window.alert = function(s) {
		$.overlay('alert-overlay', {
			value : s
		}, function() {
		});
		$('.close').next().css('top', 75);
		setTimeout(function() {
			$('.overlay').css('height', $(window).height() - 75);
		}, 1500);
	};
})(jQuery);

    
    
Date.prototype.dayOfYear= function(){
    var j1= new Date(this);
    j1.setMonth(0, 0);
    return Math.round((this-j1)/8.64e7);
};


Array.prototype.merge = function(/* variable number of arrays */) {
	for (var i = 0; i < arguments.length; i++) {
		var array = arguments[i];
		for (var j = 0; j < array.length; j++) {
			if (this.indexOf(array[j]) === -1) {
				this.push(array[j]);
			}
		}
	}
	return this;
};



String.prototype.format = function() {
	var formatted = this;
	for (var arg in arguments ) {
		formatted = formatted.replace("{" + arg + "}", arguments[arg]);
	}
	return formatted;
};
/*
 * 
		*/