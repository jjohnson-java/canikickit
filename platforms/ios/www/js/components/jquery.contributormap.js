(function($) {
	$.fn.extend({
		contibutorMap : function(data, map) {

			return this.each(function() {
				var $this = $(this);
				var currentMarker = null;

				function addMarker(lat, lng, icon){
					var latLng = new google.maps.LatLng(lat, lng);
					return new google.maps.Marker({
				      position: latLng,
				      map: map,
				      animation: google.maps.Animation.DROP,
				      icon:icon
				  	});
				}

				function processC(index){
					var item = data.contributors[index];
					var title = item.User.username.toUpperCase();
					var address = item.User.address + ' ' + item.User.city + ' ' + item.User.state + ' ' + item.User.zip;
					window.App.geocoder.geocode({
						address : address
					}, function(locResult) {
						var lat = locResult[0].geometry.location.lat();
						var lng = locResult[0].geometry.location.lng();
					  google.maps.event.addListener(addMarker(lat, lng, 'img/kiki-visited.png'), 'click', function() {
						   $($('.contributor-container')[i]).trigger('click');
						});
						if(data.contributors.length != (index + 1)){
							processC(index + 1);
						}
					});
				}

				function addUsersMarker(){
					var lat = window.App.currentPosition.coords.latitude;
					var lng = window.App.currentPosition.coords.longitude;
					addMarker(lat, lng, BASE_FULL_URL + 'timthumb.php?w=55&h=55&src=' + BASE_FULL_URL + 'featured_image/get/User/' + window.App.user.id);
				}
				if(window.App.currentPosition == null){
					navigator.geolocation.getCurrentPosition(function(currentPosition){
						window.App.currentPosition = currentPosition;
						addUsersMarker();
					}, function(e){
						$.notify("Unable to get current location.");
					}, {timeout: 20000, enableHighAccuracy: true, maximumAge: 75000});
				}else{
					addUsersMarker();
				}
			  processC(0);
			});
		}
	});
})(jQuery);


/*$.each(data.all, function(i, item){
					var title = this.name.toUpperCase();

					$.ajaxProxy({
						url : "contributors/getPlace/" + item.place_id,
						callback : function(d) {
							var latLng = new google.maps.LatLng(d.geometry.location.lat, d.geometry.location.lng);
							var marker = new google.maps.Marker({
								position : latLng,
								map : map,
								title:title,
								animation : google.maps.Animation.DROP,
								icon :'img/map-marker.png'
							});
							google.maps.event.addListener(marker, 'click', function() {
							   $($('.estiblishment-container')[i]).trigger('click');
							});
						}
					});
				});*/
