$(function() {
	window.App.view.settings.behavior = function() {
		var app = window.App;
		var user = app.user;
		$('#settings [data-role="content"]').html($.template('settings', user));

		function buildSlider($el) {
			$el.switchButton({
				width : 100,
				height : 20,
				button_width : 20
			});
		}

		function connectionString(connected) {
			return '<input type="checkbox" value="1" ' + ( connected ? 'checked' : '') + ' />';
		}

		//FACEBOOK
		$.checkFacebookStatus(function(status) {
			$('.status.facebook').html(connectionString(status));
			buildSlider($('.status.facebook input'));
		});
		$('.status.facebook .switch-button-background').click(function() {
			if ($(this).hasClass('checked')) {
				$.facebookLogin(function() {
				});
			} else {
				$.facebookLogoff(function() {
				});
			}
		});

		//GOOGLE
		$.checkGoogleStatus(function(status) {
			$('.status.google').html(connectionString(status));
			buildSlider($('.status.google input'));
		});

		$('.status.google .switch-button-background').click(function() {
			if ($(this).hasClass('checked')) {
				$.googleLogin(function() {
				});
			} else {
				$.googleLogoff(function() {
				});
			}
		});

		//TWITTER
		$.checkTwitterStatus(function(status) {
			$('.status.twitter').html(connectionString(status));
			buildSlider($('.status.twitter input'));
		});

		$('.status.twitter .switch-button-background').click(function() {
			if ($(this).hasClass('checked')) {
				$.twitterLogin(function() {
				});
			} else {
				$.twitterLogoff(function() {
				});
			}
		});

		//INSTAGRAM
		$.checkInstagramStatus(function(status) {
			$('.status.instagram').html(connectionString(status));
			buildSlider($('.status.instagram input'));
		});

		$('.status.instagram .switch-button-background').click(function() {
			if ($(this).hasClass('checked')) {
				$.instagramLogin(function() {});
			} else {
				$.instagramLogoff(function() {});
			}
		});

		function applyUser(u) {
			$("#settings .profile-image").css('background-image', 'url(' + BASE_FULL_URL + "timthumb.php?src=" + u.featured_image + '&w=100&h=100)');
			$('.username').html($.truncate(u.username.toUpperCase(), 6));

			for (var i in u) {
				var $f = $('[name="' + i + '"]');
				if ($f.attr('type') != 'file') {
					$('[name="' + i + '"]').val(u[i]);
				}
			}
		}


		$("#settings .profile-image").click(function() {
			$('#featured_image_mask').trigger('click');
			$('#featured_image').change(function() {
				$('.btnSubmit').trigger('click');
				$('#featured_image').unbind('change');
			});
		});

		$('.btnSubmit').click(function() {
			$('#profile-form').ajaxFormSubmit(function(data) {
				var obj = {};
				$.each(data, function(i, item) {
					obj[item.name] = item.value;
				});
				obj.active = 1;
				$.ajaxProxy({
					url : "/users/" + obj['id'] + ".json",
					type : 'post',
					data : obj,
					callback : function(re) {
						for (var i in obj) {
							user[i] = obj[i];
						}
						$.saveLoginDetails(user);
						applyUser(user);
						alert(window.App.view.profile.labels.profileSaved);
					}
				});
			});
		});

		applyUser(user);

		$('#featured_image_mask').click(function() {
			$('#featured_image').trigger("click");
		});

		$('#featured_image').change(function() {
			$('#featured_image_mask').val($(this).val());
		});

		$('#settings').trigger('create');

		$('#featured_image').parent().hide();

		$('#settings').trigger('create');

		return {};
	};
});
