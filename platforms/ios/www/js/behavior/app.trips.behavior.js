$(function() {
	window.App.view.trips.behavior = function() {
		$.ajaxProxy({

			url : "trips.json",
			callback : function(d) {
				var html = '';
				var dateMap = {};

				$.each(d.rows, function(i, item) {
					var p = item;
					if (dateMap[p.available_start_timestamp] == null) {
						dateMap[p.available_start_timestamp] = [];
					}
					dateMap[p.available_start_timestamp].push(item);
				});

				for (var j in dateMap) {
					var trips = dateMap[j];
					if (trips.length > 0) {
						var firstTrip = trips[0];
						html += '<div><div class="trip-header">' + $.mysqlToFormat(firstTrip['available_start_timestamp']) + ' - ' + $.mysqlToFormat(firstTrip['available_end_timestamp']) + '</div>';

						$.each(trips, function(i, item) {
							var p = item;
							html += '<div class="trip" data-id="' + p.id + '" style="background:url(' + p.featured_image + ')  !important; background-size:cover !important;width:100%;height:140px;">';
							html += '<div class="trip-name" >';
							html += p.name.toUpperCase();
							html += '</div>';
							html += '</div>';
						});
						html += '</div>';

					}
				}
				$('#rewards [data-role="content"]').html(html);

				$(".trip-header").click(function() {
					$(this).parent().find('.trip').slideToggle();
				});

				$('#rewards .trip').click(function() {
					var id = $(this).attr('data-id');
					var item = null;
					$.each(d.rows, function(i, sc) {
						if (sc.id == id) {
							item = sc;
						}
					});
					
					$.overlay('trips-overlay', {
						item : item
					}, function() {
					});
				});
			}
		});
		return {};
	};
});
