$(function() {
	window.App.view.socialmedia.behavior = function() {
		var app = window.App;
		var user = app.user;
		
		$('#challanges [data-role="content"]').html($.template('social_sharing', user));
		$(".social_sharing .profile-image").css('background-image', 'url(' + user.featured_image + ')');

		$('.share-table tr').click(function(){
			$[$(this).attr('data-share')](
				window.App.view.socialmedia.labels.defaultSharingTitle, 
				window.App.view.socialmedia.labels.defaultSharingMessage, 
				null
			);
		});
		
		function setEmpty(){
			$('.friends-section').hide();
		}
		
	    if(typeof navigator.contacts != 'undefined'){
	    	var options      = new ContactFindOptions();
			options.filter   = "";
			options.multiple = true;
			var fields = ["displayName", "name", "email"];
			navigator.contacts.find(fields, function(contacts){
				$.ajaxProxy({
					url : "users/findFriends",
					type:'post',
					data:{contacts:JSON.stringify(contacts)},
					callback : function(d) {
						if(d.length == 0){
							setEmpty();
						}else{
							$('.friends-section').slideDown();
							$('.friends-list').html($.template('contacts_that_kick_it', d));	
						}
					}
				});
			}, setEmpty, options);
		}else{
			setEmpty();
		}
		return {};
	};
});
