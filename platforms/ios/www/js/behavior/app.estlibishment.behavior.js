$(function() {
	window.App.view.estlibishment.behavior = function() {

		$.ajaxProxy({
			url : "contributors/getPlace/" + $.getUrlVar('id'),
			callback : function(d) {
				var html = "<div class='contributor-container'>";
				html += "<div style='text-align:center;'>";
				html += "<div >" + d.name + "</div>";
				html += "<div class='address' >" + d.formatted_address + "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='tel:" + d.formatted_phone_number + "' style='color:white; text-decoration:none;' >" + d.formatted_phone_number + "</a></div>";
				html += "<br/></div>";
				html += "<div style='color:#000; background:#00CFF4; text-align:center;'><span style='font-size:12px;'>" + d.website + "</span><br/>";
				html += "<div style='text-align:center; background:#2F2F2F;padding-top:15px;padding-bottom:15px;'>";
				html += "</div>";
				html += "</div>";
				html += "</div></div>";
				html += "<div class='slider'></div>";

				$('#contributor [data-role="content"]').html(html);

				$('.driving-directions').click(function() {
					
					var start = window.App.currentPosition;
					var geocoder = new google.maps.Geocoder();

					geocoder.geocode({
						address : d.formatted_address
					}, function(locResult) {
						var loc = locResult[0].geometry.location;
						var lat = loc.lat();
						var lng = loc.lng();
						var latLng = new google.maps.LatLng(lat, lng);
						var request = {
							origin : start,
							destination : latLng,
							travelMode : google.maps.TravelMode.DRIVING
						};
						var directionsService = new google.maps.DirectionsService();
						directionsService.route(request, function(response, status) {
							var leg = response.routes[0].legs[0];
							var steps = leg.steps;
							var html = "<div id='driving_map' style='height:150px;width:100%;'></div><br/>";
							html += "<div style='font-size:20px; text-align:center; text-transform:uppercase; color:#f0ff2c;'>" + leg.distance.text + " / " + leg.duration.text + "</div><br/>";
							$.each(steps, function(i) {
								html += "<div style='color:white; font-size:15px; padding:10px;'>";
								html += "<div style='color:#666;'>" + (i + 1) + ": </div>";
								html += "<div style='font-weight:normal !important;'>" + this.instructions + "</div>";
								html += "<div style='clear:both;'></div>";
								html += "</div>";
							});
							alert("<div style='text-align:left; max-height:" + ($(window).height() - 75) + "px; overflow:auto;'>" + html + "</div>");

							var directionsDisplay = new google.maps.DirectionsRenderer();
							var mapOptions = {
								zoom : 12,
								center : start,
								mapTypeId : google.maps.MapTypeId.ROADMAP,
								styles : mapStyle,
								panControl : false,
								zoomControl : false,
								mapTypeControl : false,
								scaleControl : false,
								streetViewControl : false,
								overviewMapControl : false
							};
							var map = new google.maps.Map($('#driving_map')[0], mapOptions);
							directionsDisplay.setMap(map);
							directionsDisplay.setDirections(response);
						});
					});
					
				});
				
				$('.slider').bannerSlider();
			}
		});
	}
	return {};
});


