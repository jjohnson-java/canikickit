$(function() {
	window.App.view.profile.behavior = function() {
		var app = window.App;

		var user = app.user;
		var submitDelay = null;
		
		$('#profile [data-role="content"]').html($.template('profile', user));

		function applyUser(u) {
			$("#profile .profile-image").css('background-image', 'none');
			
			$("#profile .profile-image").css('background-image', 'url(' + BASE_FULL_URL + "featured_image/get/User/" + u.id + ')');
			$('.username').html($.truncate(u.username.toUpperCase(), 6));

			for (var i in u) {
				var $f = $('[name="' + i + '"]');
				if ($f.attr('type') != 'file') {
					$('[name="' + i + '"]').val(u[i]);
				}
			}
		}

		$("#profile .profile-image").click(function(){
			$('#featured_image_mask').trigger('click');
			$('#featured_image').change(function(){
				$('.btnSubmit').trigger('click');
				$('#featured_image').unbind('change');
			});
		});

		$('.btnSubmit').click(function() {
			$('#profile-form').ajaxFormSubmit(function(data) {
				var obj = {};
				$.each(data, function(i, item) {
					obj[item.name] = item.value;
				});
				obj.active = 1;
				$.ajaxProxy({
					url : "/users/" + obj['id'] + ".json",
					type : 'post',
					data : obj,
					callback : function(re) {
						for (var i in obj) {
							user[i] = obj[i];
						}
						$.saveLoginDetails(user);
						alert(window.App.view.profile.labels.profileSaved);
						window.App.loadView('profile');
					}
				});
			});
		});

		$('.header').click(function() {
			$('#profile-form').slideToggle();
		});

		$('#profile-form').enterBind(function() {
			$('#profile').trigger('keyup');
		});

		applyUser(user);
		$('#profile').attr('data-loaded', true);

		$('#featured_image_mask').click(function() {
			$('#featured_image').trigger("click");
		});

		$('#featured_image').change(function() {
			$('#featured_image_mask').val($(this).val());
		});

		$('#profile').trigger('create');

		$('#featured_image').parent().hide();

		function loadProfileData(){
			$.ajaxProxy({
				url : "/users/profileData",
				type : 'get',
				data : {},
				callback : function(data) {
					app.currentLeaders = data.currentLeaders;
	
					$('.profile-current-top-leaders').html($.template('profile-current-leaders', {
						currentLeaders : data.currentLeaders
					}));
	
					$('.current-top-leaders, .profile-current-top-leaders').click(function(){
						alert("<div class='current-leaders-overlay'></div>");
						$('.current-leaders-overlay').html($.template('profile-current-leaders-listing', {
							currentLeaders : data.allCurrentLeaders
						}));
					});
	
					$.each(data.trips, function(i, trip) {
						if (user.number_of_bars_attended < trip.Trip.number_of_bars_needed) {
							trip.Trip.redeam_message = window.App.view.profile.labels.visitLabel + " " + trip.Trip.number_of_bars_needed + " " + window.App.view.profile.labels.barsToQualifyLabelLabel;
							trip.Trip.qualify = false;
						} else {
							trip.Trip.redeam_message = window.App.view.profile.labels.qualifyLabel;
							trip.Trip.qualify = true;
						}
					});
					$('.profile-trips').html($.template('profile-trips', {
						trips : data.trips
					}));
	
					$('.profile-trips .trip').click(function() {
						var id = $(this).attr('data-id');
						var item = null;
						$.each(data.trips, function(i, sc) {
							if (sc.Trip.id == id) {
								item = sc;
							}
						});
						$.overlay('trips-overlay', {
							item : item['Trip']
						}, function() {
						});
					});
	
					$('.weekly-promo').click(function(e) {
						e.preventDefault();
						e.stopPropagation();
						$.ajaxProxy({
							url : "sweepstakes/getCurrent",
							type : 'get',
							data : {},
							callback : function(data) {
								if(typeof data.sweepstake.Sweepstake != 'undefined'){
									alert($.template("sweepstake", data.sweepstake));
									$('.confirm-sweepstake').click(function(){
										$.ajaxProxy({
											url : "/users/enterWeeklyPromotion",
											type : 'get',
											data : {userId:user.id},
											callback : function(data) {
												alert(data.message);
											}
										});
									}).button();
								}else{
									alert(window.App.view.profile.labels.noSweepstakes);
								}
							}
						});
					});
	
					$('.share-icon').click(function() {
						app.loadView('socialmedia');
					});
				}
			});
		}
		loadProfileData();
		return {};
	};
});
