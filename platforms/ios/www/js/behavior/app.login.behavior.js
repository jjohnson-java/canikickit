$(function() {
	window.App.view.login.behavior = function() {
		var app = window.App;
		$('.menu-button').hide();
		$('[data-role="header"]').hide();
		$("[data-social-type='kickit']").click(window.App.view.login.kickitBehavior);
		$("[data-social-type='facebook']").click(window.App.view.login.facebookBehavior);
		$("[data-social-type='twitter']").click(window.App.view.login.twitterBehavior);
		$("#google-signin-button").click(window.App.view.login.googleBehavior);
		return {};
	};
	
	window.App.view.login.facebookBehavior = function(response) {
		$.facebookLogin(function(){
			openFB.api({
            	path:'/me',
            	success: function(response) {
					$.login({
						socialType : 'facebook',
						username : response.name,
						featured_image:"http://graph.facebook.com/" + response.name.replace(" ", ".") + "/picture"
					}, function() {
						window.App.loadView('profile');
					});
				}
            });
		});
	};
	
	window.App.view.login.twitterBehavior = function(response) {
		$.twitterLogin(function(response, obj){
			TWITTER.__call(
			    "users_show",
			    {screen_name: obj.screen_name},
			    function (reply) {
			        $.login({
						socialType : 'twitter',
						username : obj.screen_name,
						featured_image:reply.profile_image_url
					}, function() {
						window.App.loadView('profile');
					});
			    }
			);
		});
	};

	window.App.view.login.googleBehavior = function(authResult) {
		$.googleLogin(function(success, obj){
			if(success){
				$.login({
					socialType : 'google',
					username : obj.givenName,
					featured_image : obj.imageUrl
				}, function() {
					window.App.loadView('profile');
				});
			}
		}, false);
	};

	window.App.view.login.kickitBehavior = function() {
		window.App.loadView('register');
	};
});
