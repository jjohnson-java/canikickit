$(function() {
	window.App.view.contributor.behavior = function() {
		$.ajaxProxy({
			url : "users/" + $.getUrlVar('id') + ".json",
			callback : function(d) {
				var key = (d.user.User.username + ' / ' + d.user.User.address + ' ' + d.user.User.city + ' ' + d.user.User.state + +' ' + d.user.User.zip);

				var html = "<div class='contributor-container'>";
				html += "<div class='background' style='background-image:url(" + d.user.User.featured_image + ")'></div>";
				html += "<div style='position: relative; left: 10px; top: 10px;'>";
				html += "<div class='name' >" + d.user.User.username + "</div>";
				html += "<div class='address' >" + d.user.User.address + "&nbsp;&nbsp;|&nbsp;&nbsp;<a href='tel:" + d.user.User.phone + "' style='color:white; text-decoration:none;' >" + d.user.User.phone + "</a></div>";
				html += "<br/></div>";
				html += "<div style='color:#000; background:#00CFF4; text-indent:10px;'><span style='font-size:12px;'>Website: " + d.user.User.website + "</span><br/>";
				html += "<div style='text-align:center; background:#2F2F2F;padding-top:15px;padding-bottom:15px;'>";
				html += "<a class='view-specials-button' href='#' data-role='button' data-mini='true' data-theme='a'  data-inline='true'  >" + window.App.view.contributor.labels.barSpecialsHeader + "</a>";
				html += "<a href='javascript:void(0);' class='driving-directions' data-id='" + d.user.User.id + "' data-mini='true' data-theme='a'   data-inline='true'  data-role='button'>" + window.App.view.contributor.labels.drivingDirectionsButonLabel + "</a>";
				html += "<a class='view-menu-button' href='#' data-role='button'  data-id='" + d.user.User.id + "' data-mini='true' data-theme='a' data-inline='true'  >MENU</a>";
				html += "</div>";
				html += "</div>";
				html += "<div style='display:none;' class='bar-specials-container'><br/><div style='padding-left:10px; padding-bottom:10px; font-size:16px; color:#f0ff2c;'>" + window.App.view.contributor.labels.barSpecialsHeader + "</div>";
				$.each(d.user.Specials, function(i, item) {
					html += "<div><div class='background bar-special' style='background-image:url(" + item.featured_image + "); height:150px;'>";
					html += "<div class='special-text'>";
					html += "<h3>" + item.title + "</h3>";
					if (item.terms) {
						html += "<br/>" + item.terms;
					}
					html += "<br/></div>";
					html += "</div>";
					html += "<div class='purchase-special-button-row' data-id='" + item.id + "' >";
					html += "<div style='display: inline-block; background: none repeat scroll 0% 0% transparent; border-radius: 5px; border: 2px solid rgb(240, 255, 44); text-align: center; width: 115px; padding: 8px; font-size: 11px; color: rgb(240, 255, 44); margin-left: 20px; margin-top: 20px; '>" + window.App.view.contributor.labels.purchaseSpecialsHeader + "</div>";
					html += "<div style='margin-left:20px;display: inline-block; color: rgb(240, 255, 44);font-size:15px;' ><span style='color:white;'>EARN:</span> <img src='img/ticket-dark.png' style='vertical-align:middle;'/> x 5</div>";
					html += "</div></div>";
				});
				html += "</div>";
				html += "</div></div>";
				html += "<div class='slider'></div>";
				$('#contributor [data-role="content"]').html(html);
				$('#contributor [data-role="content"]').trigger('create');
				$('.driving-directions').click(function() {
					$.ajaxProxy({
						url : "users/" + $(this).attr('data-id') + ".json",
						callback : function(d) {
							var contributor = d.user.User;
							var start = window.App.currentPosition;
							var user = d.user.User;

							var end = user.address + ' ' + user.city + ' ' + user.state + ' ' + user.zip;
							var geocoder = new google.maps.Geocoder();

							geocoder.geocode({
								address : end
							}, function(locResult) {
								var loc = locResult[0].geometry.location;
								var lat = loc.lat();
								var lng = loc.lng();
								var latLng = new google.maps.LatLng(lat, lng);
								var request = {
									origin : start,
									destination : latLng,
									travelMode : google.maps.TravelMode.DRIVING
								};
								var directionsService = new google.maps.DirectionsService();
								directionsService.route(request, function(response, status) {
									var leg = response.routes[0].legs[0];
									var steps = leg.steps;
									var html = "<div id='driving_map' style='height:150px;width:100%;'></div><br/>";
									html += "<div style='font-size:20px; text-align:center; text-transform:uppercase; color:#f0ff2c;'>" + leg.distance.text + " / " + leg.duration.text + "</div><br/>";
									$.each(steps, function(i) {
										html += "<div style='color:white; font-size:15px; padding:10px;'>";
										html += "<div style='color:#666;'>" + (i + 1) + ": </div>";
										html += "<div style='font-weight:normal !important;'>" + this.instructions + "</div>";
										html += "<div style='clear:both;'></div>";
										html += "</div>";
									});
									alert("<div style='text-align:left; max-height:" + ($(window).height() - 75) + "px; overflow:auto;'>" + html + "</div>");

									setTimeout(function() {
										var directionsDisplay = new google.maps.DirectionsRenderer();
										var mapOptions = {
											zoom : 12,
											center : start,
											mapTypeId : google.maps.MapTypeId.ROADMAP,
											panControl : false,
											zoomControl : false,
											mapTypeControl : false,
											scaleControl : false,
											streetViewControl : false,
											overviewMapControl : false
										};
										var map = new google.maps.Map($('#driving_map')[0], mapOptions);
										directionsDisplay.setMap(map);
										directionsDisplay.setDirections(response);
									}, 1000);
								});
							});
						}
					});
				});

				function processSpecial(id) {
					function barcodeAccept(id) {
						$.ajaxProxy({
							url : "specials/purchaseSpecial/" + id,
							type : 'post',
							data : {
								contributor_id : $.getUrlVar('id'),
								userId : window.App.user.id
							},
							callback : function(d) {
								window.App.user = d.user.User;
								$.overlay('purchase-special-success-overlay', {}, function() {});
								$('img[data-share]').click(function() {
									$[$(this).attr('data-share')](window.App.view.contributor.labels.defaultSharingTitle, window.App.view.contributor.labels.defaultSharingMessage, null);
								});
							}
						});
					}

					if (cordova.plugins) {
						cordova.plugins.barcodeScanner.scan(function(result) {
							if (result.text == key) {
								barcodeAccept(id);
							} else {
								$.notify("Invalid QR code!");
							}
						}, function(error) {
							$.notify("You must supply a valid QR code to continue.");
						});
					} else {
						barcodeAccept(id);
					}
				}

				var picture = null;
				$('.purchase-special-button-row').click(function() {
					var id = $(this).attr('data-id');
					if (d.user.User.validate_location == 1) {
						var address = d.user.User.address + ' ' + d.user.User.city + ' ' + d.user.User.state + ' ' + d.user.User.zip;
						var geocoder = new google.maps.Geocoder();
						geocoder.geocode({
							address : address
						}, function(locResult) {
							var lat = locResult[0].geometry.location.lat();
							var lng = locResult[0].geometry.location.lng();
							var latLng = new google.maps.LatLng(lat, lng);
							var myLatLn = window.App.currentPosition;
							if ($.isInRange(latLng, myLatLn)) {
								processSpecial(id);
							} else {
								alert(window.App.view.contributor.labels.specialLocationError);
							}
						});
					} else {
						processSpecial(id);
					}
				});

				$('.view-specials-button').click(function() {
					$('.bar-specials-container').slideToggle();
				});

				$('.view-menu-button').click(function() {
					var id = $(this).attr('data-id');
					alert('<div style="height:500px;"><iframe src="http://docs.google.com/viewer?url=' + BASE_FULL_URL + 'users/getMenu/' + id + '&embedded=true" width="100%" height="500" style="border: none;"></iframe></div>');
					$('.close').next().css('top', 75);
				});

				$('.slider').bannerSlider();

			}
		});
	};
	return {};
});
