$(function(){
	window.App.view.passwordreset.behavior = function(){
		var app = window.App;
		var $submitBtn = $('#forgot_password .btnSubmit');
		$('.menu-button').hide();
		$submitBtn.click(function(){
			$.forgotPassword({
				email:$('#forgot_password [name="email"]').val()
			}, function(){
				alert(app.view.passwordreset.labels.passwordRestSuccess);
				app.loadView('login');
			});
		});
		$('#forgot_password').attr('data-loaded', true).enterBind(function(){
			$submitBtn.trigger('click');
		});	
		return {};		
	}
});