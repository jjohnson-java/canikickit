function IG(clientId) {
	var returnURL = "http://canikickitapp.com/system/instagram.oauth.html";
	var url = "https://api.instagram.com/oauth/authorize/?client_id=" + clientId + "&redirect_uri=" + returnURL + "&response_type=code";
	var tokenName = "INSTAGRAM_TOKEN";
	var runningInCordova = true;
	var loginWindow = null;
	var appliedCallback = null;
	
	function parseQueryString(queryString) {
		var qs = decodeURIComponent(queryString),
		    obj = {},
		    params = qs.split('&');
		params.forEach(function(param) {
			var splitter = param.split('=');
			obj[splitter[0]] = splitter[1];
		});
		return obj;
	}

	window.IGoauthCallback  = function(url) {
		var queryString,
		    obj;
		loginProcessed = true;
		if (url.indexOf("code=") > 0) {
			queryString = url.substr(url.indexOf('#') + 1);
			obj = parseQueryString(queryString);
			window.localStorage.setItem(tokenName, obj['code']);
			if(appliedCallback != null) appliedCallback(true);
		} else if (url.indexOf("error=") > 0) {
			queryString = url.substring(url.indexOf('?') + 1, url.indexOf('#'));
			obj = parseQueryString(queryString);
			if(appliedCallback != null) appliedCallback(false);
		} else {
			if(appliedCallback != null) appliedCallback(false);
		}
	};

	return {
		login : function(callback) {
			appliedCallback = callback;
			
			loginWindow = window.open(url, '_blank', 'location=no');

			if (runningInCordova) {
				function loadStart(event) {
					var url = event.url;
					if (url.indexOf("code=") > 0 || url.indexOf("error=") > 0) {
						var timeout = 600 - (new Date().getTime() - startTime);
						setTimeout(function() {
							loginWindow.close();
						}, timeout > 0 ? timeout : 0);
						window.IGoauthCallback(url);
					}
				}

				function loadExit() {
					loginWindow.removeEventListener('loadstop', loadStart);
					loginWindow.removeEventListener('exit', loadExit);
					loginWindow = null;
					window.IGoauthCallback("");
				}
				loginWindow.addEventListener('loadstart', loadStart);
				loginWindow.addEventListener('exit', loadExit);
			}
		},
		logout : function(callback) {
			window.localStorage.setItem(tokenName, null);
			callback(true);
		},
		getLoginStatus : function(callback) {
			callback(window.localStorage.getItem(tokenName) != null);
		}
	};
}
