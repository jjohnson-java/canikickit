function TWITTER(clientId, clientSecert) {
	var tokenName = "TWITTER_TOKEN";
	var loginWindow = null;
	var runningInCordova = true;
	var bird = new Codebird;
	bird.setConsumerKey(clientId, clientSecert);
	
	window.IGoauthCallback  = function(loginWindow) {
		var code = loginWindow.document.getElementsByTAgName('code');
		if(code.length == 0){
			window.localStorage.setItem(tokenName, null);
			if(appliedCallback != null) appliedCallback(false);
		}else{
			bird.__call(
			    "oauth_accessToken",
			    {oauth_verifier: code[0]},
			    function (reply) {
			        bird.setToken(reply.oauth_token, reply.oauth_token_secret);
			        window.localStorage.setItem(tokenName, reply.oauth_token);
			        appliedCallback(true, reply);
			    }
			);
		}
	};
	return {
		login : function(callback) {
			bird.__call(
			    "oauth_requestToken",
			    {oauth_callback: "oob"},
			    function (reply) {
			        bird.setToken(reply.oauth_token, reply.oauth_token_secret);
			        bird.__call(
			            "oauth_authorize",
			            {},
			            function (auth_url) {
			            	
			               loginWindow =  window.codebird_auth = window.open(auth_url, '_blank', 'location=no');
			                
			                if (runningInCordova) {
								function loadStart(event) {
									var url = event.url;
									if (url.indexOf("authorize") > 0) {
										var timeout = 600 - (new Date().getTime() - startTime);
										setTimeout(function() {
											loginWindow.close();
										}, timeout > 0 ? timeout : 0);
										window.IGoauthCallback(loginWindow);
									}
								}

								function loadExit() {
									loginWindow.removeEventListener('loadstop', loadStart);
									loginWindow.removeEventListener('exit', loadExit);
									loginWindow = null;
									window.IGoauthCallback("");
								}
								loginWindow.addEventListener('loadstart', loadStart);
								loginWindow.addEventListener('exit', loadExit);
							}
			            }
			        );
			    }
			);
		},
		logout : function(callback) {
			window.localStorage.setItem(tokenName, null);
			callback(true);
		},
		getLoginStatus : function(callback) {
			callback(window.localStorage.getItem(tokenName) != null);
		}
	};
}
