function GOOGLE(clientId, clientSecert) {
	var returnURL = "urn:ietf:wg:oauth:2.0:oob";
	var url = "https://accounts.google.com/o/oauth2/auth?" + $.param({
		client_id : clientId,
		redirect_uri : returnURL,
		response_type : 'code',
		scope : "profile"
	});

	var tokenName = "GOOGLE_TOKEN";
	var runningInCordova = true;
	var loginWindow = null;
	
	function _callback(val, obj){
		if (appliedCallback != null){
			appliedCallback(val, obj);
		}			
	}
	
	return {
		login : function(callback) {
			appliedCallback = callback;

			loginWindow = window.open(url, '_blank', 'location=no');
			
			function loadStart(event) {
				if (loginWindow.document.title.indexOf("Success") > -1) {
					var code = loginWindow.document.title.replace("Success code=");
					$.post('https://accounts.google.com/o/oauth2/token', {
						code : code,
						client_id : clientId,
						client_secret : clientSecert,
						redirect_uri : returnURL,
						grant_type : 'authorization_code'
					}).done(function(data) {
						window.localStorage.setItem(tokenName, data.access_token);
						$.getJSON("https://www.googleapis.com/plus/v1/people/me?access_token=" + data.access_token, function(obj) {
							_callback(true, obj);
						}).fail(function() {
							_callback(false);
						});
					}).fail(function(response) {
						_callback(false);
					});
					loginWindow.close();
				}else if(loginWindow.document.title.indexOf("Error") > -1){
					_callback(false);
					loginWindow.close();
				}
			}

			function loadExit() {
				loginWindow.removeEventListener('loadstop', loadStart);
				loginWindow.removeEventListener('exit', loadExit);
				loginWindow = null;
				_callback(false);
			}

			loginWindow.addEventListener('loadstart', loadStart);
			loginWindow.addEventListener('exit', loadExit);
		},
		logout : function(callback) {
			window.localStorage.setItem(tokenName, null);
			callback(true);
		},
		getLoginStatus : function(callback) {
			callback(window.localStorage.getItem(tokenName) != null);
		}
	};
}
